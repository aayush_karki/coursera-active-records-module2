# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.destroy_all
Profile.destroy_all
TodoList.destroy_all
TodoItem.destroy_all


people = ["Carly Fiorina 1954 female", "Donald Trump 1946 male", 
			"Ben Carson 1951 male", "Hillary Clinton 1947 female"]



for record in people
	words = record.split(" ")
	u = User.new
	u.username = words[1]
	u.password_digest = "xxx"
	u.save

	pro = Profile.new
	pro.gender = words[3]
	pro.birth_year = words[2]
	pro.first_name = words[0]
	pro.last_name = words[1]
	pro.user_id = u.id
	pro.save

	list = TodoList.new
	list.list_name = words[0] + " TodoList"
	list.list_due_date = Date.today + 1.year
	list.user_id = u.id
	list.save

	for i in 1..5
		item = TodoItem.new
		item.due_date = Date.today + 1.year
		item.title = words[0] + "s tile"
		item.description = "some random description"
		item.todo_list_id = list.id
		item.save

	end

end